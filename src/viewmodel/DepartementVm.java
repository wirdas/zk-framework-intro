package viewmodel;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;
import org.zkoss.zul.Textbox;

import domain.Mdepartement;


public class DepartementVm {
	
	public Mdepartement objdept;
	public List<Mdepartement> objList = new ArrayList<>();;
	
	@Wire
	private Grid grid;
	
	@Wire
	Textbox txtdeptcode,txtdeptname;

	@AfterCompose
	public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {	
		Selectors.wireComponents(view, this, false);
		doRefresh();
		objdept = new Mdepartement();
		
		grid.setRowRenderer(new RowRenderer<Mdepartement>() {

			@Override
			public void render(Row row, Mdepartement data, int index) throws Exception {
//				row.getChildren().add(new Label(String.valueOf(index++)));
//				row.getChildren().add(new Label(data.getDeptcode()));
//				row.getChildren().add(new Label(data.getDeptname()));
				
				row.appendChild(new Label(String.valueOf(index + 1)));
				row.appendChild(new Label(data.getDeptcode()));
				row.appendChild(new Label(data.getDeptname()));		
				
				Button btnRem = new Button("DELETE");
				row.appendChild(btnRem);
				btnRem.addEventListener(Events.ON_CLICK, new EventListener<Event>() {

					@Override
					public void onEvent(Event arg0) throws Exception {
						
						Messagebox.show("Apakah anda ingin menghapus list ini ?", "Konfirmasi Hapus", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener<Event>() {

							@Override
							public void onEvent(Event arg0) throws Exception {
								
								objdept = data;
								objList.remove(objdept);
								
								doRefresh();
							}
							
						});
						
					}
				});
			}
		});
	}
	
	@Command
	public void doSave() {
		objdept = new Mdepartement();
		objdept.deptcode = txtdeptcode.getValue();	
		objdept.deptname = txtdeptname.getValue();
		objList.add(objdept);
		doRefresh();
	}
	
	@Command
	public void doCancel() {
		doRefresh();
	}
	
	public void doRefresh() {
		objdept = new Mdepartement();
		grid.setModel(new ListModelList<>(objList));
		
		txtdeptcode.setValue(null);
		txtdeptname.setValue(null);
	}
	
	public Mdepartement getObjdept() {
		return objdept;
	}


	public List<Mdepartement> getObjList() {
		return objList;
	}


	public void setObjdept(Mdepartement objdept) {
		this.objdept = objdept;
	}


	public void setObjList(List<Mdepartement> objList) {
		this.objList = objList;
	}
	
}