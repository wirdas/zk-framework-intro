package domain;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class Mdepartement {
		
	public Integer mdepartementpk;
	public String deptcode;
	public String deptname;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	public Integer getMdepartementpk() {
		return mdepartementpk;
	}
	public void setMdepartementpk(Integer mdepartementpk) {
		this.mdepartementpk = mdepartementpk;
	}
	public String getDeptcode() {
		return deptcode;
	}
	public void setDeptcode(String deptcode) {
		this.deptcode = deptcode;
	}
	public String getDeptname() {
		return deptname;
	}
	public void setDeptname(String deptname) {
		this.deptname = deptname;
	}
}
